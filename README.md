## Hi there, I'm Jack 👋
I'm Jack, a Web Developer from the US! I like to code and play games in my free time.

![profile views](https://komarev.com/ghpvc/?username=jacany&style=for-the-badge&color=red)


## Projects
I'm not working on any projects currently. If I am, I will put them here. I tend to make most of what I work on Open Source unless it has a large scope or if it isn't functional.

[![trophy](https://github-profile-trophy.vercel.app/?username=jacany)](https://github.com/ryo-ma/github-profile-trophy)

## Contact
- Most of my socials are on my [Website](https://jacany.com)
- You can email me at jacany (at) chaker (dot) net
